# Extensible XP System
Extensible XP System aims for a simple XP system that you can expand upon. Quickly change XP formulas and how the XP should be calculated.


## Table of contents
1. [Requirements](#requirements)
2. [How to install](#how-to-install)
3. [How to use](#how-to-use)
4. [What's included](#whats-included)
5. [Changelog](#changelog)
6. [Credits](#credits)
7. [Feedback](#feedback)
8. [Contact & Support](#contact--support)
9. [License](#license)


## Requirements
Package has been tested on Unity version `2019.4 LTS`, should work fine in later versions as well.


## How to install
- Install using Unity Package Manager
  1. Open the Package Manager
  2. Click the "+" button at the top left of the window
  3. Select "Add package from git URL..."
  4. Enter the following URL: `https://gitlab.com/Saucyminator/unity-package-extensible-xp-system.git`
  5. Click "Add" and wait a couple of seconds for Unity to download and install the package
  6. Import the Example sample to access created ScriptableObjects for you to use
- Install by cloning the repository ([more information](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository))
- Install by downloading and importing the [.unitypackage from the Releases](https://gitlab.com/Saucyminator/unity-package-extensible-xp-system/-/releases) page
- Install by downloading it via the [Unity Asset Store](https://assetstore.unity.com/packages/slug/151021)


## How to use
1. Add `XPGranter`-component to an enemy gameobject
2. Select (or create your own) XP Grant Method asset, the project has three different methods available
3. Hook up the `GrantXP()` function on the `XPGranter`-component, for example you can call the function when the enemy dies
4. Add `XPReceiver`-component to a player gameobject
5. Select (or create your own) XP Calculation Method asset - this is where the XP gets saved
6. The UnityEvents can be used to update your UI of XP changes
7. Done! Have fun!


## What's included
| File/Folder name | Description |
| ---------------- | ----------- |
| /Documentation~ | These files & images you're currently reading. |
| /Runtime/Demo | Scripts for the demo scenes and examples. Not needed for the XP system to function. |
| /Runtime/Events | Events used by GameEvents to send Experience and Granter information. |
| /Runtime/Game events | GameEvents for easy messaging other scripts an event has happened. |
| /Runtime/RuntimeSets | ScriptableObjects that keeps a list of references that other scripts can reference on runtime. |
| /Runtime/XP/Data | Location for all ScriptableObject-scripts, I like to use the Data*-prefix for ScriptableObjects (with few exceptions). |
| /Runtime/XP/Data/XP formulas | Contains all formulas on how XP is calculated. These scripts are ScriptableObjects that you create in your project and you (or your designers) can change the values in the inspector. You can expand and create your own formula, just inherit from `DataXPFormula`.<br><br>- **Xp Per Level**: Base field that you can define however you’d like. In the receive methods I’ve created the required XP for the next level, which is increased by Xp Per Level (100).<br><br>*Example: Flat XP formula*<br>![](images/xp_formula.png)
| /Runtime/XP/Data/XP granters | ScriptableObjects that contain how XP could be granted. I've included three examples I think are the most used methods:<br><br>- **DataXPGrantAllInRuntimeSet**: Grants XP to all objects in the RuntimeSet.<br>- **DataXPGrantEvent**: Sends an XP event and if anything is listening they gain XP.<br>- **DataXPGrantInAnArea**: Checks everything within a sphere of the granter for objects that implements the IXPReceive interface and grants them XP.<br><br>*Example: DataXPGrantInAnArea*<br>![](images/xp_grant_method.png) |
| /Runtime/XP/Data/XP receivers | The ScriptableObject assets that are created from these scripts are the ones that store the XP, these scripts do the heavy lifting. Based on the XP formula selected it calculates the current XP, current level, etc.<br>In these assets is also where you assign the maximum level. A difficulty multiplier is included if you want to use it (higher value increases the XP gained). I've included two XP receiving methods that I think are the most used XP system out there:<br><br>- **Normal method**: It is similar to how World of Warcraft work. It keeps all the XP you’ve gained and just increases the required XP for the next level. It doesn't reset the current XP.<br>- **Alternative method**: It is similar to how Horizon: Zero Dawn work. It resets current XP back to zero after you gained a level and increases the required XP for the next level.<br><br>*Example: XP receive method - Normal*<br>![](images/xp_receive_method.png) |
| /Runtime/XP/Interfaces | "Contracts" of how XP must be granted and received. |
| /Runtime/XP/XPGranter.cs<br>/Runtime/XP/XPGranterEventCaller.cs | The scripts that grants XP.<br>The script could be added to an enemy and hooked up so when it dies it grants XP, just make sure to call `GrantXP()` function.<br><br> - **Experience**: The amount of XP to grant the receiver.<br>- **Granter**: Which gameobject that grants the XP.<br>- **Xp Grant Method**: Which XP Grant method that should be used when calling GrantXP(). This can easily be changed by just drag & dropping another XP Grant method in the inspector.<br><br>NOTE: `XPGranterEventCaller`-script works exactly like `XP grant method - Event` asset, I just made this script for convenience.<br><br>*Example: XP Granter prefab*<br>![](images/xp_granter.png) |
| /Runtime/XP/XPReceiver | The script that receives XP.<br>The script could be added to a player, and depending on how you are granting XP this GameObject - it receives it if the criteria is fulfilled. Has UnityEvents that you can easily hook up to UI (for example to display something when the player levels up).<br><br>- **Reset XP On Enable**: Reset the acquired XP back to zero on OnEnable.<br>- **Xp Calculation Method**: The XP calculation method that is going to save the XP, calculate current XP and level, calculate missing XP, etc.<br><br>Below are UnityEvents that you can as a response for when the XP has changed, leveled up or reached max level.<br>- **On XP Changed ()**: Is called whenever gained XP has been added to the acquired XP.<br>- **On Level Up ()**: Is called whenever a new level is reached.<br>- **On Level Max Reached ()**: Is called when maximum level is reached.<br>- **On XP Reset ()**: Is called whenever the XP is reset.<br><br>NOTE: Prefab has `Register Can Receieve XP Runtime Set` and `XP Game Event Listener`-scripts attached, both used by XP Granters.<br><br>*Example: XP Receiever prefab*![](images/xp_receiver.png) |
| /Samples~/Example/Data | ScriptableObject assets that act like data containers for:<br>- Runtime set<br>- XP formulas<br>- XP grant methods<br>- XP receive methods. |
| /Samples~/Example/Events | ScriptableObject asset for GameEvents. |
| /Samples~/Example/Prefabs | - **UI prefabs**: Used for showing how the package works.<br>- **XP prefabs**: Simple GameObjects that have the XP components. |
| /Samples~/Example/Scenes | Demo scene for showing how the package works and a demo scene for how player attacking an enemy could gain XP. |


## Changelog
### v2.0.0 (2022-01-12)
- Added: "Granter" field for choosing which gameobject is the XP granter
- Added: All other XP formulas to sample folder (previously only Flat was included)
- Added: FAQ prefab to keep information synced between the two scenes
- Added: Grant XP using GameEvents
- Added: Min() attributes to fields to reduce errors
- Added: Simple example scene and scripts how a dying enemy can grant XP to a player
- Added: Tooltips to most fields
- Added: XPListener that listens for GameEvent invokes
- Cleanup: Cleaned up code to follow my coding standards
- Cleanup: General cleanup of code
- Cleanup: Renamed folder UI to Demo
- Removed: GameObjectRuntimeSet, not being used
- Removed: Interval function from XPGranter, keeping the script a bit cleaner
- Updated: Changed text labels to TextMeshPro labels, added TextMeshPro dependency
- Updated: Debug log string interpolation
- Updated: Demo scene, moved around parts and reduced their UI size
- Updated: Dotfiles
- Updated: Events to use null-conditional operators ?.
- Updated: Expression body definition-ed some properties
- Updated: Moved radius settings to XPGrantInAnArena instead of being in the base script
- Updated: README installation steps, documentation, images
- Updated: Reduced the length of the asset creation path
- Updated: Removed prefabs from Git LFS
- Updated: Separated the RuntimeSet from XPReceiver-script to its own script
- Updated: UI canvas now scale with screen size instead of constant pixel size


### v1.0.4 (2020-06-10)
- Fixed: Warnings in the package.
- Fixed: RequiredXP not being initialized causing a "Max level reached" failure.

### v1.0.3 (2020-05-23)
- Fixed: Files are now in the example sample. Unity Package Manager made the scene read-only which made it impossible to open.

### v1.0.2 (2020-05-23)
- Updated: Installation and How-To-Use information in README and documentation files.
- Added: FAQ information in the demo scene.
- Cleanup: Re-restructure folders (again).
- Cleanup: Moved everything under the same namespace.
- Fixed: Dotfiles to help preserve .meta files.
- Removed: Unused Tests.

### v1.0.1 (2020-05-19)
- Updated: Remade everything as a Unity Package instead of a unity project (removed unnecessary files, restructured all folders, etc).
- Updated: Renamed ScriptableObject menu name from "Saucy" -> "Extensible XP System".
- Updated: Structure of the CHANGELOG file.
- Updated: Image links and minor changes to README and documentation files.

### v1.0.0 (2019-07-24)  
Public release!

- Added: Documentation both in README and a PDF.  
- Added: Credits for RuntimeSet and Square XP formula calculation.
- Fixed: Made XP grant radius field public.
- Updated: Removed duplicate XP formula method.
- Removed: Starting XP until I figured out how to use it properly.


## Credits
- RuntimeSet created by [Ryan Hipple for Unite 2017](https://github.com/roboryantron/Unite2017/blob/master/Assets/Code/Sets/RuntimeSet.cs)
- Square XP formula created by [aaaaaaaaaaaa](https://gamedev.stackexchange.com/questions/13638/algorithm-for-dynamically-calculating-a-level-based-on-experience-points/13639#13639)


## Feedback
If you have any issues please create an issue on the project's repository ([found here](https://gitlab.com/Saucyminator/unity-package-extensible-xp-system/issues)).

Suggestions or constructive criticism are appreciated. Pull requests for additional XP formulas, grant/receive methods is also welcomed.


## Contact & Support
Email: support@saucy.se  
Website: [https://saucy.se](https://saucy.se/)  
Repository url: [https://gitlab.com/Saucyminator/unity-package-extensible-xp-system](https://gitlab.com/Saucyminator/unity-package-extensible-xp-system)  
Issues: [https://gitlab.com/Saucyminator/unity-package-extensible-xp-system/issues](https://gitlab.com/Saucyminator/unity-package-extensible-xp-system/issues)  

## License
[MIT License](LICENSE.md)
