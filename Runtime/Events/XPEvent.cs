using System;
using UnityEngine;
using UnityEngine.Events;

// A customized UnityEvent with int and GameObject arguments.
// We use it to pass data from an invoker to a listener.

// int: experience
// GameObject: granter

namespace Saucy.Modules.XP {
  [Serializable]
  public class XPEvent : UnityEvent<int, GameObject> { }
}
