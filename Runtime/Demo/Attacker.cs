﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Player script.
// Simple example script for how a player gain XP by defeating an enemy.

namespace Saucy.Modules.XP {
  [RequireComponent(typeof(IXPReceive))]
  public class Attacker : MonoBehaviour {
    [Tooltip("The target we want to attack.")]
    [SerializeField] private Defender target;

    // The player that can receive XP.
    private IXPReceive player;

    private void Awake () {
      // We do GetComponent because we can't reference the interface manually via the editor.
      player = GetComponent<IXPReceive>();
    }

    public void Attack () {
      // Attack the target, passing along the player (interface) that can receive XP.
      target.TakeDamage(10, player);
    }
  }
}
