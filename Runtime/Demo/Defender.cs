﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Saucy.Modules.XP;

// Enemy script.
// Simple example script for how an enemy grants XP to a player (attacker).

namespace Saucy.Modules.XP {
  public class Defender : MonoBehaviour {
    // The enemy that can grant XP.
    private IXPGrant enemy;

    // We save a reference to the player (attacker), so when the enemy dies it can grant XP to the one that killed it.
    private IXPReceive attacker;

    private void Awake () {
      // We do GetComponent because we can't reference the interface manually via the editor.
      enemy = GetComponent<IXPGrant>();
    }

    // This is just an example function, no need to calculate HP, etc.
    public void TakeDamage (int _damage, IXPReceive _attacker) {
      Debug.Log($"Enemy taking damage");

      // We set the attacker so we can reference it later.
      attacker = _attacker;

      Die();
    }

    private void Die () {
      Debug.Log($"Enemy dies and grants XP to the attacker: {attacker}");

      // Grant XP to the attacker.
      attacker.ReceiveXP(enemy.Experience, gameObject);
    }
  }
}
