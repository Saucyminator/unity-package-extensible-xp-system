using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
//
// Author: Ryan Hipple
// Date:   10/04/17
// Url:    https://github.com/roboryantron/Unite2017/blob/master/Assets/Code/Events/GameEventListener.cs
// ----------------------------------------------------------------------------

// A script that can be added to a gameobject to listen for any invoked events it's currently subscribed to.

namespace Saucy.Modules.XP {
  public class XPGameEventListener : MonoBehaviour {
    [Tooltip("Event to listen to.")]
    public XPGameEvent Event;

    [Tooltip("Response to invoke when Event(s) are raised.")]
    public XPEvent Response;

    private void Awake () {
      if (Event == null) {
        throw new System.NullReferenceException($"No XPGameEvent asset assigned to {this}.");
      }
    }

    private void OnEnable () {
      // Register this listener to the chosen event.
      Event.RegisterListener(this);
    }

    private void OnDisable () {
      // Unregister this listener to the chosen event.
      Event.UnregisterListener(this);
    }

    public void OnEventRaised (int _experience, GameObject _granter) {
      // When an event has been invoked - this triggers the response that you can use in the editor.
      Response.Invoke(_experience, _granter);
    }
  }
}
