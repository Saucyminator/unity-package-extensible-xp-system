﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Example script how to directly subscribe/unsubscribe from an event in code.
// It's an alternative way of XPGameEventListener, in this we can directly setup the code the way we want.

namespace Saucy.Modules.XP {
  [RequireComponent(typeof(IXPReceive))]
  public class XPListener : MonoBehaviour {
    // The event we want to listen for gaining XP.
    [Header("Listening on")] [SerializeField] private XPGameEvent eventXP = null;

    // The script that can receive XP.
    private IXPReceive xpReceive;

    private void Awake () {
      // We do GetComponent because we can't reference the interface manually via the editor.
      xpReceive = GetComponent<IXPReceive>();
    }

    private void OnEnable () {
      // Whenever an event is raised, we call GrantXP() function.
      eventXP.OnEventRaised += GrantXP;
    }

    private void OnDisable () {
      // We have to unsubscribe from the event, otherwise it'll cause trouble!
      eventXP.OnEventRaised -= GrantXP;
    }

    private void GrantXP (int _experience, GameObject _granter) {
      // Whenever the event has been invoked, we just pass along the data we've gotten and send it to the ReceiveXP() function.
      xpReceive.ReceiveXP(_experience, _granter);
    }
  }
}
