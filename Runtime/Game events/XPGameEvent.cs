using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
//
// Author: Ryan Hipple
// Date:   10/04/17
// Url:    https://github.com/roboryantron/Unite2017/blob/master/Assets/Code/Events/GameEvent.cs
// ----------------------------------------------------------------------------

// A ScriptableObject asset that acts like a messenger passing along experience and granter.

namespace Saucy.Modules.XP {
  [CreateAssetMenu(menuName = "Extensible XP System/Events/XP event")]
  public class XPGameEvent : ScriptableObject {
    // Can be used to subscribe/unsubscribe from events directly in code. As an example see XPListener.
    public event UnityAction<int, GameObject> OnEventRaised;

    // Keep notes in the GameEvent asset of what the event does.
    [TextArea(3, 10)] public string devDescription = string.Empty;

    // The list of listeners that this event will notify if it is raised.
    private readonly List<XPGameEventListener> eventListeners = new List<XPGameEventListener>();

    public void Raise (int _experience, GameObject _granter) {
      // (Code) Pass along experience and granter to any subscribers.
      OnEventRaised?.Invoke(_experience, _granter);

      // (GUI) Pass along experience and granter to any subscribers.
      for (int i = eventListeners.Count - 1; i >= 0; i--) {
        eventListeners[i].OnEventRaised(_experience, _granter);
      }
    }

    public void RegisterListener (XPGameEventListener _listener) {
      if (!eventListeners.Contains(_listener)) {
        // Add XPGameEventListener to list of subscribers.
        eventListeners.Add(_listener);
      }
    }

    public void UnregisterListener (XPGameEventListener _listener) {
      if (eventListeners.Contains(_listener)) {
        // Remove XPGameEventListener from list of subscribers.
        eventListeners.Remove(_listener);
      }
    }
  }
}
