﻿using UnityEngine;

// Grants XP to all objects in a runtime set. The objects add themselves to the list on OnEnable() and remove themselves on OnDisable().
// Take a look at RegisterCanReceiveXPRuntimeSet script to register/unregister to a runtime set.

namespace Saucy.Modules.XP {
  [CreateAssetMenu(menuName = "Extensible XP System/Grant XP/All in a runtime set")]
  public class DataXPGrantAllInRuntimeSet : DataXPGrant {
    [Tooltip("Reference to a RuntimeSet asset. Fills on OnEnable with objects that can receive XP that we can loop through later.")]
    [SerializeField] protected CanReceiveXPRuntimeSet canReceiveXPSet;

    public override void GrantXP (int _experience, GameObject _granter) {
      // Loop through the runtime set and grant XP to all receivers, passing along the experience and granter.
      for (int _index = 0; _index < canReceiveXPSet.Items.Count; _index++) {
        canReceiveXPSet.Items[_index].ReceiveXP(_experience, _granter);
      }
    }
  }
}
