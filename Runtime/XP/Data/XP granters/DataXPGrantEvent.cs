﻿using UnityEngine;

// Grants XP to all objects that listens for the event. You can use either XPListener or XPGameEventListener scripts to listen for this event.
// Pros of using an event like this is there's no direct dependency between the two scripts (XP Grant and XP Receive).

namespace Saucy.Modules.XP {
  [CreateAssetMenu(menuName = "Extensible XP System/Grant XP/Event")]
  public class DataXPGrantEvent : DataXPGrant {
    [Tooltip("The event we choose to use as a messenger.")]
    [Header("Broadcasting on")] public XPGameEvent onEventGrantXP = null;

    public override void GrantXP (int _experience, GameObject _granter) {
      if (onEventGrantXP == null) {
        Debug.LogError("Asset \"{name}\" doesn't have an onEventGrantXP asset assigned.");
      }

      // Invoke the event passing along the experience and granter.
      onEventGrantXP?.Raise(_experience, _granter);
    }
  }
}
