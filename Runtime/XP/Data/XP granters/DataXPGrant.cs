﻿using UnityEngine;

// XP Grant variations. ScriptableObject assets that XPGranter-script uses to grant XP.

namespace Saucy.Modules.XP {
  public abstract class DataXPGrant : ScriptableObject {
    // The method that will grant XP with amount of experience and who grants it.
    public abstract void GrantXP (int _experience, GameObject _granter);
  }
}
