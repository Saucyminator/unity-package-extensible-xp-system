﻿using UnityEngine;

// Flat XP calculation.

namespace Saucy.Modules.XP {
  [CreateAssetMenu(menuName = "Extensible XP System/Formulas/Flat")]
  public class DataXPFormulaFlat : DataXPFormula {
    public override int Formula (int _level) => _level * xpPerLevel;
  }
}
