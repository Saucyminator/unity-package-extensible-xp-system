﻿using UnityEngine;

// XP calculation of Dungeons & Dragons 3.5 edition.

namespace Saucy.Modules.XP {
  [CreateAssetMenu(menuName = "Extensible XP System/Formulas/DnD")]
  public class DataXPFormulaDnD : DataXPFormula {
    public override int Formula (int _level) => _level * (_level - 1) * 500;
  }
}
