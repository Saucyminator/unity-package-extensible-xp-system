﻿using System.Collections;
using UnityEngine;

// This script grants XP via a XP Grant Method. The script could be attached to an enemy and hooked up that when it dies it grants XP.
// Use GrantXP() to grant XP.

namespace Saucy.Modules.XP {
  public class XPGranter : MonoBehaviour, IXPGrant {
    [Tooltip("Amount of experience to grant.")]
    [Min(1)] [SerializeField] private int experience = 10;
    public int Experience => experience;

    [Tooltip("Who/what grants the XP.")]
    [SerializeField] private GameObject granter = null;

    [Tooltip("Which XP Grant method that should be used when calling GrantXP().")]
    [Space] [SerializeField] private DataXPGrant xpGrantMethod = null;
    public DataXPGrant XPGrantMethod => xpGrantMethod;

    private void OnValidate () {
      if (granter == null) {
        Debug.LogWarning($"GameObject \"{name}\" doesn't have a Granter assigned, ignore this message if this is intended.");
      }

      // The script is missing an XP Grant method. The XP Grant method is required otherwise the script won't work.
      if (xpGrantMethod == null) {
        Debug.LogError($"GameObject \"{name}\" doesn't have a XP Grant Method asset assigned.");
      }
    }

    public void GrantXP () {
      // Calls the XP Grant method to run it's GrantXP() method. Which in turn runs the code for the chosen XP Grant method.
      XPGrantMethod.GrantXP(Experience, granter);
    }
  }
}
