﻿// An interface is a "contract" of which fields and methods the implementing scripts must have.

namespace Saucy.Modules.XP {
  public interface IXPGrant {
    int Experience { get; } // Amount of experience to be granted.
    DataXPGrant XPGrantMethod { get; } // The ScriptableObject asset that will run and check which objects that will gain XP.

    void GrantXP ();
  }
}
