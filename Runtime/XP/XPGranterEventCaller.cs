﻿using System.Collections;
using UnityEngine;

// This script grants XP via an event that other scripts can listen for. The script could be attached to an enemy and hooked up that when it dies it grants XP.
// Use GrantXP() to grant XP.

namespace Saucy.Modules.XP {
  public class XPGranterEventCaller : MonoBehaviour {
    [Tooltip("Amount of experience to grant.")]
    [Min(1)] [SerializeField] private int experience = 10;
    public int Experience => experience;

    [Tooltip("Who/what grants the XP.")]
    [SerializeField] private GameObject granter = null;

    [Tooltip("The event that acts like a messenger to tell listerners that a GrantXP event has happened.")]
    [Header("Broadcasting on")] [SerializeField] private XPGameEvent onEventGrantXP = null;

    private void OnValidate () {
      if (granter == null) {
        Debug.LogWarning($"GameObject \"{name}\" doesn't have a Granter assigned, ignore this message if this is intended.");
      }
      if (onEventGrantXP == null) {
        Debug.LogError($"GameObject \"{name}\" doesn't have a Grant XP Event asset assigned.");
      }
    }

    public void GrantXP () {
      // Sends out a GameEvent that grants XP that other gameobjects can listen for.
      onEventGrantXP?.Raise(Experience, granter);
    }
  }
}
