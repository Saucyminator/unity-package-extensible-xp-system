﻿using System.Collections.Generic;
using UnityEngine;

// ----------------------------------------------------------------------------
// Unite 2017 - Game Architecture with Scriptable Objects
//
// Author: Ryan Hipple
// Date:   10/04/17
// Url:    https://github.com/roboryantron/Unite2017/blob/master/Assets/Code/Sets/RuntimeSet.cs
// ----------------------------------------------------------------------------

// Creates an asset that on runtime adds objects to it and it can be referenced while playing.

namespace Saucy.Modules.XP {
  public abstract class RuntimeSet<T> : ScriptableObject {
    // List of objects in the RuntimeSet.
    public List<T> Items = new List<T>();

    // Add an object to the list.
    public void Add (T _thing) {
      if (!Items.Contains(_thing)) {
        Items.Add(_thing);
      }
    }

    // Remove an object from the list.
    public void Remove (T _thing) {
      if (Items.Contains(_thing)) {
        Items.Remove(_thing);
      }
    }

    // Remove all objects from the list.
    public void ClearAll () {
      if (Items.Count > 0) {
        Items.Clear();
      }
    }
  }
}
