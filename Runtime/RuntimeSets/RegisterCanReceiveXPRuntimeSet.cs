﻿using UnityEngine;
using UnityEngine.Events;

// This script sits on the gameobject that would receive XP.

namespace Saucy.Modules.XP {
  [RequireComponent(typeof(IXPReceive))]
  public class RegisterCanReceiveXPRuntimeSet : MonoBehaviour {
    [Tooltip("The RuntimeSet to add the XP Receiver to.")]
    [SerializeField] private CanReceiveXPRuntimeSet canReceiveXPSet = null;

    // The script that can receive XP.
    private IXPReceive xpReceive;

    private void Awake () {
      // We do GetComponent because we can't reference the interface manually via the editor.
      xpReceive = GetComponent<IXPReceive>();
    }

    private void OnValidate () {
      if (canReceiveXPSet == null) {
        Debug.LogError($"GameObject \"{name}\" doesn't have a CanRecieveXP runtime asset assigned.");
      }
    }

    private void OnEnable () {
      // Add this script to the list that can receive XP.
      canReceiveXPSet.Add(xpReceive);
    }

    private void OnDisable () {
      // Remove this script to the list that can receive XP.
      canReceiveXPSet.Remove(xpReceive);
    }
  }
}
