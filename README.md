# Extensible XP System
Extensible XP System aims for a simple XP system that you can expand upon. Quickly change XP formulas for how the XP should be calculated.


## Requirements
Package has been tested on Unity version `2019.4 LTS`, should work fine in later versions as well.


## How to install
- Install using Unity Package Manager
  1. Open the Package Manager
  2. Click the "+" button at the top left of the window
  3. Select "Add package from git URL..."
  4. Enter the following URL: `https://gitlab.com/Saucyminator/unity-package-extensible-xp-system.git`
  5. Click "Add" and wait a couple of seconds for Unity to download and install the package
  6. Import the Example sample to access created ScriptableObjects for you to use
- Install by cloning the repository ([more information](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository))
- Install by downloading and importing the [.unitypackage from the Releases](https://gitlab.com/Saucyminator/unity-package-extensible-xp-system/-/releases) page
- Install by downloading it via the [Unity Asset Store](https://assetstore.unity.com/packages/slug/151021)


## How to use
1. Add `XPGranter`-component to an enemy gameobject
2. Select (or create your own) XP Grant Method asset, the project has three different methods available
3. Hook up the `GrantXP()` function on the `XPGranter`-component, for example you can call the function when the enemy dies
4. Add `XPReceiver`-component to a player gameobject
5. Select (or create your own) XP Calculation Method asset - this is where the XP gets saved
6. The UnityEvents can be used to update your UI of XP changes
7. Done! Have fun!


## What's included
More in-depth documentation is [available here](Documentation~/Extensible%20XP%20System.md).


## Credits
- [RuntimeSet](https://github.com/roboryantron/Unite2017/blob/master/Assets/Code/Sets/RuntimeSet.cs) and [GameEvent](https://github.com/roboryantron/Unite2017/blob/master/Assets/Code/Events/GameEvent.cs) created by [Ryan Hipple for Unite 2017](https://github.com/roboryantron/Unite2017)
- Square XP formula created by [aaaaaaaaaaaa](https://gamedev.stackexchange.com/a/13639)


## Feedback
If you have any issues please create an issue.  
Suggestions or constructive criticism are appreciated. Pull requests for additional XP formulas, grant/receive methods is also welcomed.


## Changelog
For changes please view the [CHANGELOG](CHANGELOG.md) file.


## License
[MIT License](LICENSE.md)
