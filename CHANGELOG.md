# Changelog


## v2.0.0 (2022-01-12)
- Added: "Granter" field for choosing which gameobject is the XP granter
- Added: All other XP formulas to sample folder (previously only Flat was included)
- Added: FAQ prefab to keep information synced between the two scenes
- Added: Grant XP using GameEvents
- Added: Min() attributes to fields to reduce errors
- Added: Simple example scene and scripts how a dying enemy can grant XP to a player
- Added: Tooltips to most fields
- Added: XPListener that listens for GameEvent invokes
- Cleanup: Cleaned up code to follow my coding standards
- Cleanup: General cleanup of code
- Cleanup: Renamed folder UI to Demo
- Removed: GameObjectRuntimeSet, not being used
- Removed: Interval function from XPGranter, keeping the script a bit cleaner
- Updated: Changed text labels to TextMeshPro labels, added TextMeshPro dependency
- Updated: Debug log string interpolation
- Updated: Demo scene, moved around parts and reduced their UI size
- Updated: Dotfiles
- Updated: Events to use null-conditional operators ?.
- Updated: Expression body definition-ed some properties
- Updated: Moved radius settings to XPGrantInAnArena instead of being in the base script
- Updated: README installation steps, documentation, images
- Updated: Reduced the length of the asset creation path
- Updated: Removed prefabs from Git LFS
- Updated: Separated the RuntimeSet from XPReceiver-script to its own script
- Updated: UI canvas now scale with screen size instead of constant pixel size


## v1.0.4 (2020-06-10)
- Fixed: Warnings in the package.
- Fixed: RequiredXP not being initialized causing a "Max level reached" failure.


## v1.0.3 (2020-05-23)
- Fixed: Files are now in the example sample. Unity Package Manager made the scene read-only which made it impossible to open.


## v1.0.2 (2020-05-23)
- Updated: Installation and How-To-Use information in README and documentation files.
- Added: FAQ information in the demo scene.
- Cleanup: Re-restructure folders (again).
- Cleanup: Moved everything under the same namespace.
- Fixed: Dotfiles to help preserve .meta files.
- Removed: Unused Tests.


## v1.0.1 (2020-05-19)
- Updated: Remade everything as a Unity Package instead of a unity project (removed unnecessary files, restructured all folders, etc).
- Updated: Renamed ScriptableObject menu name from "Saucy" -> "Extensible XP System".
- Updated: Structure of the CHANGELOG file.
- Updated: Image links and minor changes to README and documentation files.


## v1.0.0 (2019-07-24)  
Public release!

- Added: Documentation both in README and a PDF.  
- Added: Credits for RuntimeSet and Square XP formula calculation.
- Fixed: Made XP grant radius field public.
- Updated: Removed duplicate XP formula method.
- Removed: Starting XP until I figured out how to use it properly.


## v0.1.1 (2019-07-23)
- Fixed: Code now compatible with versions v5.6.7f1 and v2017.4.30f1 ([2ecbf2c8](https://gitlab.com/Saucyminator/unity-package-extensible-xp-system/commit/2ecbf2c8a7d1347bba436e17d75de0f0fa20987a))
  - **NOTE**: For versions v5.6.7f1 or v2017.4.30f1 the prefabs does not work because of Unity's new prefab workflow introduced in v2018.3. Code runs fine though. If you are running these versions just skip importing the prefabs.


## v0.1.0 (2019-07-23)
- Initial commit.
- Five different XP formulas available.
- Two different XP grant methods available.
- Two different XP receive methods available (where XP is saved).
